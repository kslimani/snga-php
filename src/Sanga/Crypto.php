<?php

namespace Sanga;

class Crypto
{
    const DELIMITER = '$';
    const METHOD = 'aes-256-cbc';
    const BAD_KEY = 'Invalid key';
    const BAD_KEY_P1 = 'Invalid key part 1';
    const BAD_KEY_P2 = 'Invalid key part 2';
    const BAD_TOKEN = 'Invalid token';

    /**
     * Create random application key.
     *
     * @return string The random key
     */
    public static function createAppKey()
    {
        return bin2hex(openssl_random_pseudo_bytes(16))
            .self::DELIMITER
            .bin2hex(openssl_random_pseudo_bytes(32));
    }

    /**
     * Create random application id.
     *
     * @return string The random application id
     */
    public static function createAppId()
    {
        return bin2hex(openssl_random_pseudo_bytes(6));
    }

    /**
     * Create "final" application key used with Token class.
     *
     * @param string $appId  The application id
     * @param string $appKey The application key
     *
     * @return string The random application id
     */
    public static function createKey($appId, $appKey)
    {
        return $appId
            .self::DELIMITER
            .$appKey;
    }

    /**
     * Split key.
     *
     * @param string $key The key
     *
     * @throws InvalidArgumentException if the key is invalid
     *
     * @return array The splited key
     */
    protected static function splitKey($key)
    {
        $k = explode(self::DELIMITER, $key);

        if (count($k) !== 2) {
            throw new \InvalidArgumentException(self::BAD_KEY);
        }

        // IV must be 32 hexadecimal bytes long (16 binary bytes long)
        if (mb_strlen($k[0]) !== 32 || !ctype_xdigit($k[0])) {
            throw new \InvalidArgumentException(self::BAD_KEY_P1);
        }

        // Key must be 64 hexadecimal bytes long (32 binary bytes long)
        if (mb_strlen($k[1]) !== 64 || !ctype_xdigit($k[1])) {
            throw new \InvalidArgumentException(self::BAD_KEY_P2);
        }

        return $k;
    }

    /**
     * Encrypt a string with key.
     *
     * @param string $string The string to encrypt
     * @param string $key    The key
     *
     * @throws InvalidArgumentException if the key is invalid
     *
     * @return string|false The token string, otherwise false on error
     */
    public static function encrypt($string, $key)
    {
        list($iv, $key) = self::splitKey($key);

        $encrypted = openssl_encrypt(
            $string,
            self::METHOD,
            hex2bin($key),
            OPENSSL_RAW_DATA,
            hex2bin($iv)
        );

        return $encrypted === false ? false : bin2hex($encrypted);
    }

    /**
     * Decrypt a token string with key.
     *
     * @param string $token The token string to decrypt
     * @param string $key   The key
     *
     * @throws InvalidArgumentException if token or key is invalid
     *
     * @return string|false The decrypted string, otherwise false on error
     */
    public static function decrypt($token, $key)
    {
        list($iv, $key) = self::splitKey($key);

        if (!ctype_xdigit($token)) {
            throw new \InvalidArgumentException(self::BAD_TOKEN);
        }

        return openssl_decrypt(
            hex2bin($token),
            self::METHOD,
            hex2bin($key),
            OPENSSL_RAW_DATA,
            hex2bin($iv)
        );
    }
}
