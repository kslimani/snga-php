<?php

namespace Sanga;

class Token
{
    const TIME = '_t';
    const ID = '_id';

    /**
     * Create token string required by javascript player.
     *
     * @param string $key  The application key
     * @param string $id   The video source identifier
     * @param array  $data Optionnal key-value array
     *
     * @return string The encrypted token string
     */
    public static function create($key, $id, array $data = [])
    {
        // Extract application id from key
        $array = explode(Crypto::DELIMITER, $key);
        $appId = array_shift($array);

        // Encrypt and append application id to token
        return $appId
            .Crypto::DELIMITER
            .Crypto::encrypt(
                self::createData($id, $data),
                implode(Crypto::DELIMITER, $array)
            );
    }

    /**
     * Create data string to be encrypted.
     *
     * @param string $id   The video source identifier
     * @param array  $data The key-value data array
     *
     * @return string The JSON encoded string
     */
    protected static function createData($id, $data)
    {
        return json_encode([
            self::TIME => microtime(true),
            self::ID => $id,
        ] + $data);
    }

    /**
     * Converts token string to data array.
     *
     * @param string $appKey The application key
     * @param string $token  The encrypted token string
     *
     * @return array The decrypted data array
     */
    public static function toArray($appKey, $token)
    {
        return json_decode(
            Crypto::decrypt($token, $appKey),
            true
        );
    }
}
