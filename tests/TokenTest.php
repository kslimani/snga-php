<?php

namespace Sanga\Tests;

use Sanga\Crypto;
use Sanga\Token;

class TokenTest extends \PHPUnit_Framework_TestCase
{
    protected $videoId = 'azerty123';
    protected $appId;
    protected $appKey;
    protected $key;

    protected function setUp()
    {
        $this->appKey = Crypto::createAppKey();
        $this->appId = Crypto::createAppId();
        $this->key = Crypto::createKey($this->appId, $this->appKey);
    }

    public function testCreate()
    {
        // Created token must not equals false
        $token = Token::create($this->key, $this->videoId);
        $this->assertNotFalse($token);

        // First token part must be application id
        $parts = explode(Crypto::DELIMITER, $token);
        $this->assertCount(2, $parts);
        $this->assertEquals($this->appId, $parts[0]);

        sleep(1); // Wait 1 sec. to ensure token differs

        // Create again must produce a different token
        $again = Token::create($this->key, $this->videoId);
        $this->assertNotFalse($again);
        $this->assertNotEquals($token, $again);
    }

    public function testToArray()
    {
        // Second token part is encrypted token
        $token = Token::create($this->key, $this->videoId);
        $parts = explode(Crypto::DELIMITER, $token);

        $array = Token::toArray($this->appKey, $parts[1]);
        $this->assertNotfalse($array);
        $this->assertArrayHasKey(Token::TIME, $array);
        $this->assertArrayHasKey(Token::ID, $array);

        // Decrypted array contains video id
        $this->assertEquals($array[Token::ID], $this->videoId);
    }

    public function testToArrayWithData()
    {
        $customData = [
            'foo' => 'bar',
            Token::ID => 'xxx', // Overwrite attempt
        ];

        // Second token part is encrypted token
        $token = Token::create($this->key, $this->videoId, $customData);
        $parts = explode(Crypto::DELIMITER, $token);

        // Decrypted array contains custom data
        $array = Token::toArray($this->appKey, $parts[1]);
        $this->assertArrayHasKey('foo', $array);
        $this->assertEquals($array['foo'], $customData['foo']);

        // And the video id is not overwritten by 'xxx' value
        $this->assertEquals($array[Token::ID], $this->videoId);
    }
}
