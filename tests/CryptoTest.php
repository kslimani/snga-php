<?php

namespace Sanga\Tests;

use Sanga\Crypto;

class CryptoTest extends \PHPUnit_Framework_TestCase
{
    public function testCreateAppKey()
    {
        $appKey = Crypto::createAppKey();
        $this->assertNotEmpty($appKey);
        $this->assertCount(2, explode(Crypto::DELIMITER, $appKey));
    }

    public function testCreateAppId()
    {
        $appId = Crypto::createAppId();
        $this->assertNotEmpty($appId);
        $this->assertEquals(12, mb_strlen($appId));
    }

    public function testCreateKey()
    {
        $appId = Crypto::createAppId();
        $appKey = Crypto::createAppKey();
        $key = Crypto::createKey($appId, $appKey);
        $this->assertNotEmpty($key);
        $this->assertCount(3, explode(Crypto::DELIMITER, $key));
    }

    public function testCrypto()
    {
        $appKey = Crypto::createAppKey();
        $data = 'this is a test !';

        $token = Crypto::encrypt($data, $appKey);
        $this->assertNotFalse($token);

        $result = Crypto::decrypt($token, $appKey);
        $this->assertEquals($data, $result);
    }

    public function testBadkey()
    {
        $appKey = 'badkey';

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage(Crypto::BAD_KEY);
        $token = Crypto::encrypt('some data', $appKey);
    }

    public function testBadkeyPart1()
    {
        $appKey = Crypto::createAppKey();
        $appKey = mb_substr($appKey, 4);

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage(Crypto::BAD_KEY_P1);
        $token = Crypto::encrypt('some data', $appKey);
    }

    public function testBadkeyPart2()
    {
        $appKey = Crypto::createAppKey();
        $appKey = mb_substr($appKey, 0, -4);

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage(Crypto::BAD_KEY_P2);
        $token = Crypto::encrypt('some data', $appKey);
    }

    public function testBadToken()
    {
        $appKey = Crypto::createAppKey();

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage(Crypto::BAD_TOKEN);
        $token = Crypto::decrypt('bad.token', $appKey);
    }
}
