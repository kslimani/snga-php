# Sanga PHP

Generate token required by javascript.

# Usage

```php
<?php
    use Sanga\Token;

    $appKey = "secret";  // Shared secret key
    $videoId = "azerty"; // Could be retrieved from database

    // Generate token string
    $token = Token::create($appKey, $videoId);

    // Optionally, custom key-value data array can also be injected in token
    $token = Token::create($appKey, $videoId, [ 'foo' => 'bar' ]);
```

The length of the token varies according token content. Injecting a large data array may generate a token with an excessive length.
